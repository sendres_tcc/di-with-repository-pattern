﻿using System.Data.Entity;
using DependencyProject.Models;

namespace DependencyProject.Context
{
    public class DPContext: DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }
}