﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DependencyProject.Enums
{
    public enum AccountTypeEnum
    {
        Admin = 1,
        Client = 2
    }
}