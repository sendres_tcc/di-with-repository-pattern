﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DependencyProject.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}