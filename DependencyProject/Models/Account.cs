﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DependencyProject.Models
{
    public class Account
    {
        public int AccountId { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [EmailAddress]
        [Display(Name = "Email")]
        [StringLength(500)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string  Password { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string LastName { get; set; }
        public int AccountTypeId { get; set; }

    }
}