﻿using DependencyProject.Models;
using DependencyProject.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DependencyProject.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IService<Customer> _customerService = null;
        public CustomerController(IService<Customer> customerService)
        {
            _customerService = customerService;
        }
        public ActionResult Index()
        {
            var customers = _customerService.FindAll();
            return View(customers);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            _customerService.Create(customer);
            return RedirectToAction("Index");
        }
    }
}