﻿using DependencyProject.Models;
using DependencyProject.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DependencyProject.Services
{
    public class CustomerService : IService<Customer>
    {
        private readonly ICustomerRepository _iCustomerRepository;
        public CustomerService(ICustomerRepository customerRepository)
        {
            _iCustomerRepository = customerRepository;
        }
        public int Create(Customer customer)
        {
            return _iCustomerRepository.Create(customer);
        }
        public IEnumerable<Customer> FindAll()
        {
            return _iCustomerRepository.FindAll();
        }
        public Customer FindById(int id)
        {
            return _iCustomerRepository.FindById(id);
        }
        public void SendMail()
        {
            //implement here..
        }
        public void CalculateTotalProfit()
        {
            //implement here..
        }
    }
}