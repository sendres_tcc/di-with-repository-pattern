﻿using DependencyProject.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DependencyProject.Repositories
{
    public abstract class BaseRepository<T> : IRepository<T> where T : class
    {
        protected DPContext _context = null;
        protected DbSet<T> _DbSet { get; set; }

        public BaseRepository()
        {
            _context = new DPContext();
            _DbSet = _context.Set<T>();
        }

        public List<T> GetAll()
        {
            return _DbSet.ToList();
        }

        public T Get (int? id)
        {
            return _DbSet.Find(id);
        }

        public void Add(T pEntity)
        {
            _DbSet.Add(pEntity);
        }

        public void Edit(T pEntity)
        {
            _context.Entry(pEntity).State = EntityState.Modified;
        }

        public void Remove(T pEntity)
        {
            _DbSet.Remove(pEntity);
        }

        public void Dispose()
        {
            //DbSet.Dispose();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}