﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DependencyProject.Repositories
{
    public interface IRepository<T> where T : class
    {
        List<T> GetAll();

        T Get(int? id);

        void Add(T pEntity);

        void Edit(T pEntity);

        void Remove(T pEntity);

        void Dispose();

        void SaveChanges();
    }
}