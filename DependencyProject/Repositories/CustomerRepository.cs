﻿using DependencyProject.Context;
using DependencyProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DependencyProject.Repositories
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public int Create(Customer customer)
        {
            if (customer != null)
            {
                base._context.Customers.Add(customer);
                _context.SaveChanges();
                return 1;
            }
            return 0;
        }
        public IEnumerable<Customer> FindAll()
        {
            return _context.Customers.ToList();
        }
        public Customer FindById(int id)
        {
            return _context.Customers.Find(id);
        }
    }
}