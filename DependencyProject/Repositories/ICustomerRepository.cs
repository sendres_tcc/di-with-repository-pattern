﻿using DependencyProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DependencyProject.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        int Create(Customer customer);
        IEnumerable<Customer> FindAll();
        Customer FindById(int id);
    }
}