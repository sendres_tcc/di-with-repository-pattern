﻿namespace DependencyProject.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DependencyProject.Context.DPContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DependencyProject.Context.DPContext context)
        {
            context.Accounts.AddOrUpdate(x => x.AccountId,
                new Models.Account()
                {
                    AccountId = 1,
                    Email = "system@admin.com",
                    Password = "C73D5A53596CF91809D0D5B926C183CC",
                    FirstName = "System",
                    LastName = "Admin",
                    AccountTypeId = 1,
                } //Password is Admin1010
            );

            context.Customers.AddOrUpdate(x => x.CustomerId,
                new Models.Customer()
                {
                    CustomerId = 1,
                    Name = "asd",
                    LastName = "asd",
                },
                new Models.Customer()
                {
                    CustomerId = 2,
                    Name = "zxc",
                    LastName = "zxc",
                },
                new Models.Customer()
                {
                    CustomerId = 3,
                    Name = "qwe",
                    LastName = "qwe",
                }
            );
        }
    }
}
